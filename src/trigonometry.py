from .config import trigonometryConfig, anglesConfig, PI
from math import sin, cos, tan, degrees, radians


def sinus(angle):
    return anglesConfig[str(angle)]["sin"] if str(angle) in anglesConfig else sin(radians(angle))


def cosinus(angle):
    return anglesConfig[str(angle)]["cos"] if str(angle) in anglesConfig else cos(radians(angle))


def tangens(angle):
    return anglesConfig[str(angle)]["tan"] if str(angle) in anglesConfig else tan(radians(angle))


def cotangens(angle):
    return anglesConfig[str(angle)]["cot"] if str(angle) in anglesConfig else 1 / tan(radians(angle))


def trigonometry_menu():
    print("Выберите действие:")
    for i in range(len(trigonometryConfig)):
        print(f"{i + 1}. {trigonometryConfig[i]}")
    choose = int(input("Ваш выбор: "))
    if choose == 1:
        angle = int(input("Введите угол в градусах: "))
        print(sinus(angle))
        input("Для продолжения нажмите клавишу enter...")
    elif choose == 2:
        angle = int(input("Введите угол в градусах: "))
        print(cosinus(angle))
        input("Для продолжения нажмите клавишу enter...")
    elif choose == 3:
        angle = int(input("Введите угол в градусах: "))
        print(tangens(angle))
        input("Для продолжения нажмите клавишу enter...")
    elif choose == 4:
        angle = int(input("Введите угол в градусах: "))
        print(cotangens(angle))
        input("Для продолжения нажмите клавишу enter...")
    elif choose == 5:
        angle = input("Введите угол в формате (3p/2): ")
        result = convert(angle)
        if result:
            print(f'{angle} = {int(round(result))}')
        else:
            print("Неверный ввод")
        input("Для продолжения нажмите клавишу enter...")
    elif choose == 6:
        angle = int(input("Введите угол: "))
        result = anglesConfig[str(angle)]["rad"] if str(angle) in anglesConfig else round(radians(angle), 3)
        print(result)
        input("Для продолжения нажмите клавишу enter...")
    elif choose == len(trigonometryConfig):
        return


def convert(angle):
    result = float(1)
    is_numerator = True
    for i in angle:
        if i.isdigit():
            if is_numerator:
                result *= int(i)
            else:
                result /= int(i)
        elif i == "p":
            if is_numerator:
                result *= PI
            else:
                result /= PI
        elif i == "/":
            if is_numerator:
                is_numerator = False
        else:
            return False
    return round(degrees(result))
