import pytest

from src.trigonometry import *
from src.config import trigonometryConfig

is_in_convert = False
i = 0
is_angle = False


def angle_error(*args, **kwargs):
    global is_angle
    if is_angle:
        return '3/2p*'
    else:
        is_angle = True
        return 5


def change_input(*args, **kwargs):
    global i
    global is_in_convert
    print(i)
    print(is_in_convert)
    if not is_in_convert or i + 1 > 5:
        result = i + 1
    else:
        result = '3p/2'
    if i + 1 == 5:
        is_in_convert = True
    else:
        is_in_convert = False
    return result


def test_convert_whole_returns_ok():
    assert convert('2p') == 360


def test_convert_part_returns_ok():
    assert convert('3p/4') == 135


def test_convert_part_devide_p():
    assert convert('2/3p') == 12


def test_convert_part_devide_fault():
    assert not convert('2/3p*')


def test_sinus_returns_ok():
    assert sinus(0) == "0"


def test_menu_first(monkeypatch):
    global i
    while i < len(trigonometryConfig):
        monkeypatch.setattr('builtins.input', change_input)
        assert trigonometry_menu() is None
        i += 1


def test_menu_value_error(monkeypatch):
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: 'mother')
        trigonometry_menu()


def test_menu_angle_error(monkeypatch):
    monkeypatch.setattr('builtins.input', angle_error)
    trigonometry_menu()


def test_sinus_value_error():
    assert sinus(90) == "1"


def test_sinus_negvalue_error():
    assert sinus(-90) == -1


def test_sinus_isint():
    assert isinstance(sinus(150), float)


def test_sinus_borders():
    for angle in range(720):
        print(angle, str(angle) in anglesConfig)
        if str(angle) in anglesConfig:
            continue
        else:
            assert 1 >= float(sinus(angle)) >= -1
