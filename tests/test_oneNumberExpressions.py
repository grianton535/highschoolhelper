import pytest

from src.oneNumberExpressions import *
from src.config import oneNumberExpressionsConfig

count = 0


def raise_input(*args, **kwargs):
    global count

    if count == 0:
        count += 1
        return 5
    else:
        return 1


def test_factorial_returns_ok():
    assert factorial(6) == 720


def test_degree_returns_ok():
    assert raise_to_degree(17, 2) == 289


def test_sqrt_returns_ok():
    assert sqrt(16) == 4


def test_is_prime_returns_ok():
    assert is_prime(17)


def test_is_prime_returns_error():
    assert not is_prime(2014)


def test_menu_first(monkeypatch):
    for i in range(len(oneNumberExpressionsConfig)):
        monkeypatch.setattr('builtins.input', lambda _: int(i + 1))
        assert one_number_menu() is None


def test_menu_value_error(monkeypatch):
    with pytest.raises(ValueError):
        monkeypatch.setattr('builtins.input', lambda _: 'mother')
        one_number_menu()


def test_menu_natural(monkeypatch):
    monkeypatch.setattr('builtins.input', raise_input)
    assert one_number_menu() is None
